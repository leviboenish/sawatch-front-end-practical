import React from "react";
import { CircleMarker, MapContainer, TileLayer } from "react-leaflet";

export default function PracticalMap(props){

  const { data } = props;

  function parkingMarks() {
    return data.map(pl => {
      return(
        <CircleMarker
          key={pl.uuid}
          center={[pl.lat, pl.lon]}
          color={"black"}
          radius={10}
        >
        </CircleMarker>
      );
    });
  };

  return(
    <div>
      <MapContainer
        className = "map-container"
        center={[39.79208,-105.08285]}
        zoom={13}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
          url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
          maxZoom={20}
        />
        {parkingMarks()}
      </MapContainer>
    </div>);
}
