import React from "react";
import { useTable, useSortBy } from "react-table7";

export default function PracticalTable(props) {
  const { columns, data } = props;

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  },
    useSortBy
  );

  data.forEach((item, index) => {
    if (item.assetId === '') {
      item.assetId = item.vin
    }
    let makeCopy = item.make.split('-')
    console.log('make', makeCopy.join('-'))
    makeCopy.forEach((word, index) => {
      let lower = word.toLowerCase();
      makeCopy[index] = word.charAt(0).toUpperCase() + lower.slice(1);
    })
    item.make = makeCopy.join('-')
  })

  return (
    <div>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>{column.render("Header")}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? ' 🔽'
                        : ' 🔼'
                      : ''}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {rows.map(row => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>);
}
