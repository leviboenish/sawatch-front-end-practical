import React from "react";
import { Line } from "react-chartjs-2";

export default function PracticalGraph(props){
    const { data }= props;
    const labels = data.map((r) => { return `${r.month} ${r.year}` });
    const miles = data.map((r) => { return parseFloat(r.miles) });
    const trips = data.map((r) => { return parseInt(r.count) });
    const ghgLbs = data.map((r) => { return parseFloat(r.ghgLbs) });
    const dataSet = 
      {
        labels: labels,
        datasets: [
          {
            label: "Miles",
            data: miles,
            backgroundColor: "#A0DDFF"
          },
          {
            label: "GHG",
            data: ghgLbs,
            backgroundColor: "#624CAB"
          },
          {
            label: "Trips",
            data: trips,
            backgroundColor: "#758ECD"
          }
        ],
      }

      return(
        <div>
          <Line 
            className="graph-container"
            id="graph"
            height={500}
            data={dataSet}
            options={{
                hover: {
                mode: "dataset",
                intersect: true,
                },
                maintainAspectRatio: false,
                scales: {
                yAxes: [
                    {
                    ticks: {
                        display: true,
                        suggestedMax: 10,
                        suggestedMin: 0,
                    },
                    },
                ],
                xAxes: [
                    {
                    ticks: {
                        display: false,
                        suggestedMax: 10,
                        suggestedMin: 0,
                    },
                    },
                ],
                },
            }}>
          </Line>
      </div>);
}
